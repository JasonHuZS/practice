{-# OPTIONS --without-K --safe #-}

module SumN.README where

-- static semantics
import SumN.Statics

-- nbe operations for βη equivalence
import SumN.Semantics
-- completeness for nbe
import SumN.PER
-- soundness for nbe
import SumN.Soundness
